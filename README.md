

**Command to create a and run Docker.**

First we must create a web application where we create our dockerfile file and we must save it in a git repo.

-Then we enter the remote machine.
   
    ssh <user> @ dc-dev-ceo-pground -p 2300

-When we enter the remote machine we proceed to Clone the repository, the https url is copied Example:

    git clone https: //smunoz@bitbucket.org/smunoz/acme.git

-When the Project is cloned, we proceed to give docker build
    
    Docker build . -t <user> / <repo>:latest

-And we finished running the project with
   
    docker run -d --name <application> <user> / <application>